package com.testproject.bwtest;

public class ResalePlan {

    String mdn;
    String resalePlan;

    public ResalePlan(){}

    public ResalePlan(String mdn, String resalePlan){
        this.mdn = mdn;
        this.resalePlan = resalePlan;
    }

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getResalePlan() {
        return resalePlan;
    }

    public void setResalePlan(String resalePlan) {
        this.resalePlan = resalePlan;
    }
}
