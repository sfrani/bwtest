package com.testproject.bwtest;

public class CarrierPlan {

    String customer;
    String mdn;
    String sprintPlan;
    String socs;

    public CarrierPlan(){}

    public CarrierPlan(String customer, String mdn, String sprintPlan, String socs){
        this.customer = customer;
        this.mdn = mdn;
        this.sprintPlan = sprintPlan;
        this.socs = socs;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getSprintPlan() {
        return sprintPlan;
    }

    public void setSprintPlan(String sprintPlan) {
        this.sprintPlan = sprintPlan;
    }

    public String getSocs() {
        return socs;
    }

    public void setSocs(String socs) {
        this.socs = socs;
    }
}
