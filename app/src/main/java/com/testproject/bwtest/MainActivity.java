package com.testproject.bwtest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private String TAG = "PermissionLOG";
    private static int PICK_RESALE_CODE = 101;
    private static int PICK_CARRIER_CODE = 102;
    private ArrayList<ResalePlan> resaleArray;
    private ArrayList<CarrierPlan> carrierArray;
    private TextView resaleFileName;
    private TextView carrierFileName;
    private boolean readingDone = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isStoragePermissionGranted();

        resaleArray = new ArrayList<>();
        carrierArray = new ArrayList<>();

        Button button = findViewById(R.id.button);
        button.setOnClickListener(v -> chooseFile(PICK_RESALE_CODE));

        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(v -> chooseFile(PICK_CARRIER_CODE));

        resaleFileName = findViewById(R.id.textView);
        carrierFileName = findViewById(R.id.textView2);

        Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(v -> {
            if(resaleArray.size() > 0 && carrierArray.size() > 0 && readingDone){
                new WriteCsvAsync().execute();
            }
            else {
                Toast.makeText(MainActivity.this, getString(R.string.upload_error), Toast.LENGTH_SHORT).show();
            }
        });

        }

    private void chooseFile(int code){
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.setType("*/*");
        String[] mimetypes = {"text/csv", "text/comma-separated-values", "application/csv"};
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(chooseFile, code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {
            CsvReaderHelper csvReaderHelper = new CsvReaderHelper(data.getData(), requestCode);
            new ReadCsvAsync().execute(csvReaderHelper);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    private void initFolder(){
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "BWTest";
        File folder = new File(baseDir);
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    private void initFirstRow(String fileName, String[] firstRow){
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "BWTest";

        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath);
        CSVWriter writer;

        try {
            if (f.exists() && !f.isDirectory()) {
                FileWriter mFileWriter = null;
                mFileWriter = new FileWriter(filePath, true);
                writer = new CSVWriter(mFileWriter);
            } else {
                writer = new CSVWriter(new FileWriter(filePath));
            }

                writer.writeNext(firstRow);

            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void writeToCsv(String fileName, String[] line) throws IOException {
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "BWTest";
        String filePath = baseDir + File.separator + fileName;
        File f = new File(filePath);
        CSVWriter writer;

        try {
            if (f.exists() && !f.isDirectory()) {
                FileWriter mFileWriter = null;
                mFileWriter = new FileWriter(filePath, true);
                writer = new CSVWriter(mFileWriter);
            } else {
                writer = new CSVWriter(new FileWriter(filePath));
            }

            writer.writeNext(line);

            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private class ReadCsvAsync extends AsyncTask<CsvReaderHelper, Integer, Integer>{

        String csvFileName = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            readingDone = false;
        }

        @Override
        protected Integer doInBackground(CsvReaderHelper... csvReaderHelpers) {
            CsvReaderHelper csvReaderHelper = csvReaderHelpers[0];
            Uri uri = csvReaderHelper.getUri();
            File csvFile = new File(Objects.requireNonNull(uri.getPath()).replace(":", "/"));
            csvFileName = csvFile.getName();

            try {
                InputStream csvStream = getContentResolver().openInputStream(uri);
                assert csvStream != null;
                InputStreamReader csvStreamReader = new InputStreamReader(csvStream);
                CSVReader csvReader = new CSVReader(csvStreamReader);
                String[] line;

                csvReader.readNext();

                if(csvReaderHelper.getCode() == PICK_RESALE_CODE) {
                    while ((line = csvReader.readNext()) != null) {
                        resaleArray.add(new ResalePlan(line[0], line[1]));
                    }
                }
                if(csvReaderHelper.getCode() == PICK_CARRIER_CODE){
                    while ((line = csvReader.readNext()) != null) {
                        carrierArray.add(new CarrierPlan(line[0], line[1], line[2], line[3]));
                    }
                }
            } catch (IOException | CsvValidationException | IndexOutOfBoundsException e) {
                e.printStackTrace();
            }

            return csvReaderHelper.getCode();
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if(integer == PICK_CARRIER_CODE) carrierFileName.setText(csvFileName);
            if(integer == PICK_RESALE_CODE) resaleFileName.setText(csvFileName);
            readingDone = true;
            super.onPostExecute(integer);
        }
    }


   private class WriteCsvAsync extends AsyncTask<Void, Integer, String> {
        String TAG = getClass().getSimpleName();
       private ProgressDialog dialog;

       protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage(getString(R.string.creating_files));
            dialog.show();
        }

        protected String doInBackground(Void...arg0) {
            int zeroPlan = 0;
            int tenPlan = 0;
            int twentyfivePlan = 0;
            int fiftyPlan = 0;
            int hundredPlan = 0;
            int fivehundredPlan = 0;
            boolean firstRowWritten = false;

            initFolder();
            initFirstRow("Document1.csv", new String[] {"MDN", "Resale Plan", "Sprint Plan", "SOCs"});
            initFirstRow("Document2.csv", new String[] {"MDN", "Resale Plan", "Sprint Plan", "LTE SOC"});
            initFirstRow("Document3.csv", new String[] {"Resale Plan", "Number of Devices"});

            for(int i=0; i<resaleArray.size(); i++){
                int progress = 100*i / resaleArray.size();
                publishProgress(progress);
                ResalePlan currentResaleItem = resaleArray.get(i);

                String rp = currentResaleItem.getResalePlan();
                //DOCUMENT3
                    switch (rp){
                        case "MRC-0M": {
                            zeroPlan++;
                            break;
                        }
                        case "MRC-10M": {
                            tenPlan++;
                            break;
                        }
                        case "MRC-25M": {
                            twentyfivePlan++;
                            break;
                        }
                        case "MRC-50M": {
                            fiftyPlan++;
                            break;
                        }
                        case "MRC-100M": {
                            hundredPlan++;
                            break;
                        }
                        case "MRC-500M": {
                            fivehundredPlan++;
                            break;
                        }
                    }


                for(int j=0; j<carrierArray.size(); j++){
                    CarrierPlan currentCarrierItem = carrierArray.get(j);

                    String mdn = currentResaleItem.getMdn();
                    String resalePlan = currentResaleItem.getResalePlan();
                    String sprintPlan = currentCarrierItem.getSprintPlan();
                    String socs = currentCarrierItem.getSocs();

                    if(currentResaleItem.getMdn().equals(currentCarrierItem.getMdn())){


                        //DOCUMENT1
                        try {
                            writeToCsv("Document1.csv", new String[] {mdn, resalePlan, sprintPlan, socs});
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //DOCUMENT2
                        try {
                            if (socs.contains("DSMLTESOC")) {
                                writeToCsv("Document2.csv", new String[]{mdn, resalePlan, sprintPlan, "Y"});
                            } else {
                                writeToCsv("Document2.csv", new String[]{mdn, resalePlan, sprintPlan, "N"});
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            }

            try {
                writeToCsv("Document3.csv", new String[]{"MRC-0M", Integer.toString(zeroPlan)});
                writeToCsv("Document3.csv", new String[]{"MRC-10M", Integer.toString(tenPlan)});
                writeToCsv("Document3.csv", new String[]{"MRC-25M", Integer.toString(twentyfivePlan)});
                writeToCsv("Document3.csv", new String[]{"MRC-50M", Integer.toString(fiftyPlan)});
                writeToCsv("Document3.csv", new String[]{"MRC-100M", Integer.toString(hundredPlan)});
                writeToCsv("Document3.csv", new String[]{"MRC-500M", Integer.toString(fivehundredPlan)});
            } catch (Exception e){
                e.printStackTrace();
            }

            return "You are at PostExecute";
        }

       @Override
       protected void onProgressUpdate(Integer... values) {
           super.onProgressUpdate(values);
           String filesLocation = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "BWTest";
           dialog.setMessage(getString(R.string.creating_files) + System.lineSeparator() + getString(R.string.files_location) + filesLocation + System.lineSeparator() + getString(R.string.progress) + values[0] + "%");
       }

       protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

}
