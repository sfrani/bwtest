package com.testproject.bwtest;

import android.net.Uri;

public class CsvReaderHelper {

    private Uri uri;
    private int code;

    public CsvReaderHelper(Uri uri, int code){
        this.uri = uri;
        this.code = code;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
